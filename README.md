*** This repository is the web implementation of the following papers: ***

1) Hien To, Gabriel Ghinita, Cyrus Shahabi. PrivGeoCrowd: A Toolbox for Studying Private Spatial Crowdsourcing (demo). 40th International Conference on Very Large Data Bases (ICDE 2015)

2) Hien To, Gabriel Ghinita, Cyrus Shahabi. A Framework for Protecting Worker Location Privacy in Spatial Crowdsourcing. In Proceedings of the 40th International Conference on Very Large Data Bases (VLDB 2014)

Related studies:

https://bitbucket.org/hto/privategeocrowd/

https://bitbucket.org/hto/geocrowd

https://bitbucket.org/hto/maximumcomplextask/